package vocabularyplusfxmaven.vocabularyPlus;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import vocabularyplusfxmaven.vocabularyPlus.Controller.MainController;
import vocabularyplusfxmaven.vocabularyPlus.Controller.SettingsController;
import vocabularyplusfxmaven.vocabularyPlus.Model.Settings;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            // keep window inside screen bounds
            Bounds allScreenBounds = computeAllScreenBounds();
            ChangeListener<Number> boundsListener = (obs, oldValue, newValue) -> {
                double x = primaryStage.getX();
                double y = primaryStage.getY();
                double w = primaryStage.getWidth();
                double h = primaryStage.getHeight();
                if (x < allScreenBounds.getMinX()) {
                    primaryStage.setX(allScreenBounds.getMinX());
                }
                if (x + w > allScreenBounds.getMaxX()) {
                    primaryStage.setX(allScreenBounds.getMaxX() - w);
                }
                if (y < allScreenBounds.getMinY()) {
                    primaryStage.setY(allScreenBounds.getMinY());
                }
                if (y + h > allScreenBounds.getMaxY()) {
                    primaryStage.setY(allScreenBounds.getMaxY() - h);
                }
            };
            primaryStage.xProperty().addListener(boundsListener);
            primaryStage.yProperty().addListener(boundsListener);
            primaryStage.widthProperty().addListener(boundsListener);
            primaryStage.heightProperty().addListener(boundsListener);

            primaryStage.setTitle("Vocabulary+");

            FXMLLoader fxmlLoader = new
                    FXMLLoader(getClass().getClassLoader().getResource("UI/MainView.fxml"));
            Parent root = fxmlLoader.load();
            MainController mainController = fxmlLoader.getController();
            mainController.setMainStage(primaryStage);

            Settings settings = mainController.settings;
            primaryStage.setWidth(
                    settings.get(settings.COLS_COUNT).getValue() *
                    settings.get(settings.CELL_SIZE).getValue() + 20
            );
            primaryStage.setHeight(
                    settings.get(settings.ROWS_COUNT).getValue() *
                    settings.get(settings.CELL_SIZE).getValue() + 90
            );

            primaryStage.setMinWidth(100);
            primaryStage.setMinHeight(200);
            primaryStage.initStyle(StageStyle.DECORATED);
            primaryStage.setScene(new Scene(root));
            primaryStage.show();

            primaryStage.setOnCloseRequest(event ->
            {
                SQLiteDB.save(mainController.millisecondToMinutes(System.currentTimeMillis()));
                System.exit(0);
            });
        } catch(NullPointerException e) {
            System.out.println("Exception: " + e.getMessage()+ "\r\n");
            if (e.getMessage().equals("Location is required.")) {
                System.out.println("Looks like View location is incorrect.\r\n" +
                        "FXMLLoader cant load this .fxml file.\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bounds computeAllScreenBounds() {
        double minX = Double.POSITIVE_INFINITY;
        double minY = Double.POSITIVE_INFINITY;
        double maxX = Double.NEGATIVE_INFINITY;
        double maxY = Double.NEGATIVE_INFINITY;
        for (Screen screen : Screen.getScreens()) {
            Rectangle2D screenBounds = screen.getBounds();
            if (screenBounds.getMinX() < minX) {
                minX = screenBounds.getMinX();
            }
            if (screenBounds.getMinY() < minY) {
                minY = screenBounds.getMinY();
            }
            if (screenBounds.getMaxX() > maxX) {
                maxX = screenBounds.getMaxX();
            }
            if (screenBounds.getMaxY() > maxY) {
                maxY = screenBounds.getMaxY();
            }
        }
        return new BoundingBox(minX, minY, maxX-minX, maxY-minY);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
