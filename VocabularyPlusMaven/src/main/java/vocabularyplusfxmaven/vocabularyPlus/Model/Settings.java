package vocabularyplusfxmaven.vocabularyPlus.Model;

import java.util.HashMap;

/**
 * Created by Vladimir on 25.11.2014.
 *
 */
public class Settings extends HashMap<String, Setting> {
    public final String COLS_COUNT           = "cols_count";
    public final String ROWS_COUNT           = "rows_count";
    public final String LEFT_LANGUAGE        = "left_language";
    public final String RIGHT_LANGUAGE       = "right_language";
    public final String AUTOLOAD_ON_OS_START = "autoload_on_OS_start";
    public final String CELL_SIZE            = "cell_size";
}
