package vocabularyplusfxmaven.vocabularyPlus.Model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by ubihinon on 30.09.15.
 */
public class Translation {
    private Integer id;
    private SimpleStringProperty text;
    private SimpleStringProperty comment;
    private Integer langId;

    public Translation() {}

    public Translation(Integer id, String text, String comment, Integer langId) {
        this.id = id;
        this.text = new SimpleStringProperty(text);
        this.comment = new SimpleStringProperty(comment);
        this.langId = langId;
    }

    /*
     * GETTERS AND SETTERS
     */

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text.get();
    }

    public SimpleStringProperty textProperty() {
        return text;
    }

    public void setText(String text) {
        this.text.set(text);
    }

    public String getComment() {
        return comment.get();
    }

    public SimpleStringProperty commentProperty() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment.set(comment);
    }

    public Integer getLangId() {
        return langId;
    }

    public void setLangId(Integer langId) {
        this.langId = langId;
    }
}
