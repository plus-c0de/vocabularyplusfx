package vocabularyplusfxmaven.vocabularyPlus.Model;

import vocabularyplusfxmaven.vocabularyPlus.SQLiteDB;

/**
 * Created by Vladimir on 29.03.2015.
 *
 */
public class WordShow {
    private Integer wordId;
    private long showedTime;
    private Integer showCount;

    public WordShow(Integer wordId, long showedTime, Integer showCount) {
        this.wordId     = wordId;
        this.showedTime = showedTime;
        this.showCount  = showCount;
    }

    public boolean update() {
        SQLiteDB.update(this);
        return true;
    }

    /*
     * GETTERS AND SETTERS
     */

    public Integer getWordId() {
        return wordId;
    }

    public long getShowedTime() {
        return showedTime;
    }

    public void setShowedTime(long showedTime) {
        this.showedTime = showedTime;
    }

    public Integer getShowCount() {
        return showCount;
    }

    public void setShowCount(Integer showCount) {
        this.showCount = showCount;
    }
}
