package vocabularyplusfxmaven.vocabularyPlus.Model;

import vocabularyplusfxmaven.vocabularyPlus.SQLiteDB;

import java.util.ArrayList;

/**
 * Created by Vyacheslav on 2/16/2015.
 * This is class savePicture, update, delete data for Group
 */
public class Group {
    private Integer id;
    private String name;
    private ArrayList<Group> subGroups;
    private ArrayList<Word> words;

    public Group() {}
    public Group(Integer id, String name) {
        this.id   = id;
        this.name = name;
    }

    public void initWords() {
        words = new ArrayList<>();
    }
    public void initSubGroups() {
        subGroups = new ArrayList<>();
    }

    public boolean update(Language language) {
        SQLiteDB.update(this, language);
        return true;
    }

    public boolean delete() {
        SQLiteDB.delete(this);
        return true;
    }

    public boolean saveTo(Group rootGroup, Language language) {
        SQLiteDB.insert(this, rootGroup, language);
        return true;
    }

    // returns string of subgroups id separated by comma
    public String getSubGroupsId() {
        StringBuilder builder = new StringBuilder();
        recursiveMakeGroupIds(this, builder);
        return builder.toString().replaceFirst(",", "");
    }

    private void recursiveMakeGroupIds(Group group, StringBuilder str) {
        if(group.getId() != 1) {
            str.append(",").append(group.getId());
            if(group.getSubGroups() != null) {
                for (int groupI = 0; groupI < group.getSubGroups().size(); groupI++) {
                    Group nextGroup = group.getSubGroups().get(groupI);
                    recursiveMakeGroupIds(nextGroup, str);
                }
            }
        }
    }

    /*
     * GETTERS AND SETTERS
     */

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Group> getSubGroups() {
        return subGroups;
    }

    public ArrayList<Word> getWords() {
        return words;
    }
}
