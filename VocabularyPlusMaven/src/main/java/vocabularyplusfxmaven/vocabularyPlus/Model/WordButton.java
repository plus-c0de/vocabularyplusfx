package vocabularyplusfxmaven.vocabularyPlus.Model;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import vocabularyplusfxmaven.vocabularyPlus.Controller.SettingsController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Vladimir on 09.03.2015.
 *
 */
public class WordButton extends Button {
    private Word wordLink;
    private Tooltip tooltip;

    public WordButton() {
        setPrefSize(100, 100);
        setAlignment(Pos.TOP_CENTER);
        setWrapText(true);
        tooltip = new Tooltip();
        tooltip.setId("tooltipWordButton");
        tooltip.setMinSize(300, 100);
        setTooltip(tooltip);
    }

    public Background getImage() throws IOException {
        Background background;
        if(wordLink.getPicture() != null) {
            BackgroundSize backgroundSize = new BackgroundSize(this.getWidth(),
                    this.getHeight(), true, true, true, false);
            BackgroundImage backgroundImage = new BackgroundImage(
                    byteArrayToImage(wordLink.getPicture()),
                    BackgroundRepeat.NO_REPEAT,
                    BackgroundRepeat.NO_REPEAT,
                    BackgroundPosition.CENTER, backgroundSize
            );
            background = new Background(backgroundImage);
        } else {
            background = setBackground();
        }
        return background;
    }

    public Background setBackground() {
        return new Background(
                new BackgroundFill(Paint.valueOf("#00aff5"), CornerRadii.EMPTY, Insets.EMPTY)
        );
    }

    //convert byte[] to javafx Image
    private Image byteArrayToImage(byte[] picture) throws IOException {
//        if(picture == null) {
//            throw new IOException("Empty image");
//        }
        InputStream inputStream = new ByteArrayInputStream(picture);
        BufferedImage bufferedImage = ImageIO.read(inputStream);

//        BufferedImage img = bufferedImage.getSubimage(bufferedImage.getWidth() / 2, bufferedImage.getHeight() / 2,
//                bufferedImage.getWidth() / 2, bufferedImage.getHeight() / 2);

        Image image = SwingFXUtils.toFXImage(bufferedImage, null);

//        JOptionPane.showMessageDialog(null, "", "", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(img));
        return image;
    }

    private void fillTooltip() {
        StringBuilder builder = new StringBuilder();
/*        for(Word word : this.wordLink.getTranslations()) {
            builder.append(word.getText());
            *//*if(word.getComment() != null) {
                builder.append("\t");
                builder.append(word.getComment());
            }*//*
            builder.append("\n");
        }*/
        tooltip.setText(builder.toString());
    }

    public Word getWordLink() {
        return wordLink;
    }

    public void setWordLink(Word wordLink) {
        this.wordLink = wordLink;
        fillTooltip();
    }
}
