package vocabularyplusfxmaven.vocabularyPlus.Model;

import vocabularyplusfxmaven.vocabularyPlus.SQLiteDB;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Vladimir on 18.11.2014.
 *
 */
public class Language {
    private Integer id;
    private SimpleStringProperty shortName;
    private SimpleStringProperty name;

    public Language() {
        this.shortName = new SimpleStringProperty();
        this.name      = new SimpleStringProperty();
    }

    public Language(Integer id, String shortName, String name) {
        this.id         = id;
        this.shortName  = new SimpleStringProperty(shortName);
        this.name       = new SimpleStringProperty(name);
    }

    public boolean save() {
        SQLiteDB.insert(this);
        return true;
    }

    public boolean update() {
        SQLiteDB.update(this);
        return true;
    }

    public boolean delete() {
        SQLiteDB.delete(this);
        return true;
    }

    /*
     * GETTERS AND SETTERS
     */

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName.get();
    }

    public void setShortName(String shortName) {
        this.shortName.set(shortName);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }
}
