package vocabularyplusfxmaven.vocabularyPlus.Model;

import vocabularyplusfxmaven.vocabularyPlus.SQLiteDB;

/**
 * Created by Vladimir on 25.11.2014.
 *
 */
public class Setting {
    private Integer id;
    private String shortName;
    private String name;
    private Integer value;
    private Integer defaultValue;

    public Setting(Integer id, String shortName, String name, Integer value, Integer defaultValue) {
        this.id             = id;
        this.shortName      = shortName;
        this.name           = name;
        this.value          = value;
        this.defaultValue   = defaultValue;
    }

    public boolean save() {
        SQLiteDB.update(this);
        return true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Integer defaultValue) {
        this.defaultValue = defaultValue;
    }
}
