package vocabularyplusfxmaven.vocabularyPlus.Model;

import vocabularyplusfxmaven.vocabularyPlus.SQLiteDB;

import java.util.ArrayList;

/**
 * Created by Vyacheslav on 2/16/2015.
 *
 */
public class Word {
    private Integer id;
    private String text;
    private String comment;
    private byte[] picture;
    private int langId;
    private ArrayList<Word> translations;

    public Word() {}

    public Word(Integer id, String text, int langId) {
        this.id     = id;
        this.text   = text;
        this.langId = langId;
    }

    public Word(Integer id, String text, String comment, byte[] picture, int langId) {
        this.id      = id;
        this.text    = text;
        this.comment = comment;
        this.picture = picture;
        this.langId  = langId;
    }

    public void initTranslations() {
        translations = new ArrayList<>();
    }

    // save word in group
    public boolean saveTo(Group group) {
        SQLiteDB.insert(this, group);
        return true;
    }

    // save translation for word2 and savePicture in group
    public boolean saveTo(Word word2, Group group) {
        SQLiteDB.insert(this, word2, group);
        return true;
    }

    // save picture
    public boolean savePicture() {
        SQLiteDB.insertPicture(this);
        return true;
    }

    public boolean update() {
        SQLiteDB.update(this);
        return true;
    }

    // delete word from wordGroup
    public boolean deleteFrom() {
        SQLiteDB.delete(this);
        return true;
    }

    /*
     * GETTERS AND SETTERS
     */

    public ArrayList<Word> getTranslations() {
        return translations;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public int getLangId() {
        return langId;
    }

    public void setLangId(int langId) {
        this.langId = langId;
    }
}
