package vocabularyplusfxmaven.vocabularyPlus.Controller;

import javafx.scene.Node;
import javafx.scene.control.Slider;
import javafx.stage.Stage;
import vocabularyplusfxmaven.vocabularyPlus.Model.Setting;
import vocabularyplusfxmaven.vocabularyPlus.Model.Settings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import vocabularyplusfxmaven.vocabularyPlus.Model.WordButton;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Vladimir on 12.02.2015.
 *
 */
public class SettingsController implements Initializable {
    @FXML private Label countWords;
    @FXML private ComboBox<Integer> rowComboBox;
    @FXML private ComboBox<Integer> columnComboBox;
    @FXML private CheckBox startWithOS;
    @FXML private Slider cellSize;
    private Settings settings;
    private Setting setting;
    private ObservableList<Node> wordButtons;
    private Stage mainStage;
    private MainController mainController;

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<Integer> data = FXCollections.observableArrayList();
        data.addAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        rowComboBox.setItems(data);
        columnComboBox.setItems(data);

        actions();
    }

    //set values for interface components
    private void init() {
        // set values for rowComboBox and columnComboBox
        rowComboBox.setValue(settings.get(settings.ROWS_COUNT).getValue());
        columnComboBox.setValue(settings.get(settings.COLS_COUNT).getValue());
        setCountWords();

        // set values for startWithOS
        boolean value = true;
        if(settings.get(settings.AUTOLOAD_ON_OS_START).getValue() == 0) {
            value = false;
        }
        startWithOS.setSelected(value);
    }

    private void actions() {
        rowComboBox.setOnAction(event -> {
            setting = settings.get(settings.ROWS_COUNT);
            setting.setValue(rowComboBox.getValue());
            setting.save();
            setCountWords();

            mainController.updateCellsMainWindow();
            setSizeWordButtons();
            setSizeMainStage();
        });

        columnComboBox.setOnAction(event -> {
            setting = settings.get(settings.COLS_COUNT);
            setting.setValue(columnComboBox.getValue());
            setting.save();
            setCountWords();

            mainController.updateCellsMainWindow();
            setSizeWordButtons();
            setSizeMainStage();
        });

        startWithOS.setOnAction(event -> {
            setting = settings.get(settings.AUTOLOAD_ON_OS_START);
            setting.setValue(startWithOS.isSelected() ? 1 : 0);
            try {
                if (setting.getValue() == 1) {
                    Runtime.getRuntime().exec("cmd /c start build.bat");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            setting.save();
        });

        cellSize.valueProperty().addListener((observable, oldValue, newValue) -> {
            setSizeWordButtons();
            setSizeMainStage();
            settings.get(settings.CELL_SIZE).setValue((int) cellSize.getValue());
            settings.get(settings.CELL_SIZE).save();
        });
    }

    private void setCountWords() {
        if(rowComboBox.getValue() != null && columnComboBox.getValue() != null) {
            countWords.setText(Integer.toString(rowComboBox.getValue() * columnComboBox.getValue()));
        }
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
        init();
    }

    public void setWordButtons(ObservableList<Node> wordButtons) {
        this.wordButtons = wordButtons;
    }

    private void setSizeWordButtons() {
        for (Node wordButton : wordButtons) {
            ((WordButton) wordButton).setPrefSize(cellSize.getValue(), cellSize.getValue());
        }
    }

    private void setSizeMainStage() {
        mainStage.setWidth(columnComboBox.getValue() * cellSize.getValue() + 20);
        mainStage.setHeight(rowComboBox.getValue() * cellSize.getValue() + 90);
    }

    public Stage getMainStage() {
        return mainStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    public MainController getMainController() {
        return mainController;
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }
}
