package vocabularyplusfxmaven.vocabularyPlus.Controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import vocabularyplusfxmaven.vocabularyPlus.Model.Group;
import vocabularyplusfxmaven.vocabularyPlus.Model.Language;
import vocabularyplusfxmaven.vocabularyPlus.Model.Settings;
import vocabularyplusfxmaven.vocabularyPlus.Model.Word;
import vocabularyplusfxmaven.vocabularyPlus.SQLiteDB;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.util.StringConverter;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by Vyacheslav on 2/13/2015.
 *
 */
public class TranslationController implements Initializable {
    @FXML private ChoiceBox<Language> leftLangChoiceBox;
    @FXML private Button switchLangButton;
    @FXML private ChoiceBox<Language> rightLangChoiceBox;
    @FXML private TreeView<Group> groupsTreeView;
    @FXML private TableView<TableWord> translationsTableView;
    @FXML private TableColumn<TableWord, String> originalColumn;
    @FXML private TableColumn<TableWord, String> translationColumn;
    @FXML private TableColumn<TableWord, String> commentColumn;
    private ArrayList<Integer> currentlySelectedGroups;
    private ObservableList<TableWord> data = FXCollections.observableArrayList();
    private Group selectedGroup;
    private Settings settings;

    @FXML
    public void initialize(URL location, ResourceBundle resourceBundle) {

        translationsTableView.setItems(data);
        translationsTableView.setEditable(true);
        // LOL, there is no disableColumnReordering() method. So ugly code below disables it
        translationsTableView.getColumns().
                addListener(new ListChangeListener<TableColumn<TableWord, ?>>() {
                    public boolean suspended;

                    @Override
                    public void onChanged(Change<? extends TableColumn<TableWord, ?>> c) {
                        c.next();
                        if (c.wasReplaced() && !suspended) {
                            this.suspended = true;
                            translationsTableView.getColumns().setAll(originalColumn,
                                    translationColumn, commentColumn);
                            this.suspended = false;
                        }
                    }
                });

        groupsTreeView.setEditable(true);
        groupsTreeView.setCellFactory(param -> new TreeEditingCell());

        originalColumn.setCellValueFactory(
                new PropertyValueFactory<>("original")
        );
        originalColumn.setCellFactory(param -> new EditingCell());

        translationColumn.setCellValueFactory(
                new PropertyValueFactory<>("translation")
        );
        translationColumn.setCellFactory(param -> new EditingCell());

        commentColumn.setCellValueFactory(
                new PropertyValueFactory<>("comment")
        );
        commentColumn.setCellFactory(param -> new EditingCell());
    }

    private void buildData() {
        // clear data
        data.clear();
        groupsTreeView.setRoot(null);

        // Getting groups
        Group rootGroup = SQLiteDB.getGroups(settings);

        // Group Tree
        TreeItem<Group> rootItem = new TreeItem<>(rootGroup);
        groupsTreeView.setRoot(rootItem);
        recursiveMakeGroupTree(rootItem);

        // Table Data
        selectedGroup = rootGroup.getSubGroups().get(0);
//        selectedGroup = rootGroup;
        makeData(selectedGroup);
        data.add(new TableWord(null, null, true));

        groupsTreeView.getSelectionModel().select(1);
    }

    private void initLanguageBoxes() {
        ObservableList<Language> languages = SQLiteDB.getLanguages();

        leftLangChoiceBox.setConverter(new StringConverter<Language>() {
            @Override
            public String toString(Language object) {
                return object.getName();
            }

            @Override
            public Language fromString(String string) {
                return null;
            }
        });
        rightLangChoiceBox.setConverter(new StringConverter<Language>() {
            @Override
            public String toString(Language object) {
                return object.getName();
            }

            @Override
            public Language fromString(String string) {
                return null;
            }
        });
        leftLangChoiceBox.setItems(languages);
        rightLangChoiceBox.setItems(languages);
        Language leftSelectedLanguage = languages.get(0),
                 rightSelectedLanguage = languages.get(0);

        // lol, finding...
        for (Language language : languages) {
            if (language.getId().equals(settings.get(settings.LEFT_LANGUAGE).getValue())) {
                leftSelectedLanguage = language;
            } else if (language.getId().equals(settings.get(settings.RIGHT_LANGUAGE).getValue())) {
                rightSelectedLanguage = language;
            }
        }
        // Select language on start.
        leftLangChoiceBox.getSelectionModel().select(leftSelectedLanguage);
        rightLangChoiceBox.getSelectionModel().select(rightSelectedLanguage);

        // User changed selection..
        leftLangChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            // if user trying to select left lang which already chosen for right lang
            // else set new language settings value & rebuild forms
            if (newValue != null && newValue.equals(rightLangChoiceBox.getSelectionModel().getSelectedItem())) {
                leftLangChoiceBox.getSelectionModel().select(oldValue);
            } else if (newValue != null) {
                settings.get(settings.LEFT_LANGUAGE).setValue(newValue.getId());
                settings.get(settings.LEFT_LANGUAGE).save();
                buildData();
            }
        });
        rightLangChoiceBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            // if user trying to select right lang which already chosen for left lang
            // else set new language settings value & rebuild forms
            if (newValue != null && newValue.equals(leftLangChoiceBox.getSelectionModel().getSelectedItem())) {
                rightLangChoiceBox.getSelectionModel().select(oldValue);
            } else if (newValue != null) {
                settings.get(settings.RIGHT_LANGUAGE).setValue(newValue.getId());
                settings.get(settings.RIGHT_LANGUAGE).save();
                buildData();
            }
        });

        // Switch language button action
        switchLangButton.setOnAction(event -> {
            final Language leftLang = leftLangChoiceBox.getSelectionModel().getSelectedItem();
            final Language rightLang = rightLangChoiceBox.getSelectionModel().getSelectedItem();
            leftLangChoiceBox.getSelectionModel().clearSelection();
            rightLangChoiceBox.getSelectionModel().clearSelection();
            leftLangChoiceBox.getSelectionModel().select(rightLang);
            rightLangChoiceBox.getSelectionModel().select(leftLang);
        });
    }

    private void recursiveMakeGroupTree(TreeItem<Group> currentItem) {
        if(currentItem.getValue().getSubGroups() != null) {
            currentItem.setExpanded(true);
            for (int groupI = 0; groupI < currentItem.getValue().getSubGroups().size(); groupI++) {
                TreeItem<Group> nextItem = new TreeItem<>(
                        currentItem.getValue().getSubGroups().get(groupI)
                );
                currentItem.getChildren().add(nextItem);
                recursiveMakeGroupTree(nextItem);
            }
        }
    }

    private void recursiveMakeGroupTree(TreeItem<Group> currentItem, ArrayList<Integer> idGroups) {
        System.out.println("currentItem = " + currentItem.getValue().getId());

        if(currentItem.getValue().getSubGroups() != null) {
            for (int groupI = 0; groupI < currentItem.getValue().getSubGroups().size(); groupI++) {
                TreeItem<Group> nextItem = new TreeItem<>(
                        currentItem.getValue().getSubGroups().get(groupI)
                );

                if (currentItem.getValue().getId() != 0 /*|| currentItem.getValue().getId() != 1*/) {
                     boolean remove = currentItem.getChildren().remove(currentItem);
                    System.out.println(remove);
//                currentItem.getParent().getChildren().remove(currentItem);
                }
                recursiveMakeGroupTree(nextItem, idGroups);
            }
        }
    }

    private void makeData(Group currentGroup) {
        if (currentGroup.getWords() != null) {
            Word currentOriginal;
            Word currentTranslation;
            for (int originalI = 0; originalI < currentGroup.getWords().size(); originalI++) {
                currentOriginal = currentGroup.getWords().get(originalI);
                currentTranslation = currentOriginal.getTranslations().get(0);
                data.add(new TableWord(currentOriginal, currentTranslation, true));
                for (int translationI = 1;
                     translationI < currentOriginal.getTranslations().size();
                     translationI++) {
                    currentTranslation = currentOriginal.getTranslations().get(translationI);
                    data.add(new TableWord(currentOriginal, currentTranslation, false));
                }
            }
        }
    }

    private void recursiveMakeData(Group currentGroup) {
        makeData(currentGroup);
        if (currentGroup.getSubGroups() != null) {
            for (int groupI = 0; groupI < currentGroup.getSubGroups().size(); groupI++) {
                recursiveMakeData(currentGroup.getSubGroups().get(groupI));
            }
        }
    }

    // Somehow this class handles data updating and saving. Looks like okay, i think
    public class TableWord {
        private Word originalLink;
        private Word translationLink;
        private boolean showOriginal; // now this determines show data in first column or not
        private SimpleStringProperty original;
        private SimpleStringProperty translation;
        private SimpleStringProperty comment;

        public TableWord(Word originalLink, Word translationLink, boolean showOriginal) {
            this.originalLink = originalLink;
            this.translationLink = translationLink;
            this.showOriginal = showOriginal;
            original = new SimpleStringProperty();
            translation = new SimpleStringProperty();
            comment = new SimpleStringProperty();

            if (originalLink != null && showOriginal) {
                original.set(originalLink.getText());
            }
            if (translationLink != null) {
                translation.set(translationLink.getText());
                if (translationLink.getComment() != null
                        && !translationLink.getComment().isEmpty()) {
                    comment.set(translationLink.getComment());
                }
            }
        }

        public void deleteOriginal() {
            originalLink.deleteFrom();
        }
        public void deleteTranslation() {
            translationLink.deleteFrom();
            translationLink = null;
            translation.set("");
            comment.set("");

            // trick to update columns data, LOL
            translationColumn.setVisible(false);
            translationColumn.setVisible(true);
            commentColumn.setVisible(false);
            commentColumn.setVisible(true);
        }


        public Word getOriginalLink() {
            return originalLink;
        }
        public Word getTranslationLink() {
            return translationLink;
        }
        public String getOriginal() {
            return original.get();
        }

        // get pictures from the internet
        private class ImageThread implements Runnable {
            Word word;
            Thread thread;

            public ImageThread() {}

            @Override
            public void run() {
                try {
                    word.setPicture(getImage(word.getText()));
                    word.savePicture();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void execute(Word word) {
                this.word = word;
                this.thread = new Thread(this, "ImageThread");
                this.thread.start();
            }

            private byte[] getImage(String text) throws Exception {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

                System.out.println("url = " + URLEncoder.encode(text, "UTF-8"));

                URL url = new URL("https://ajax.googleapis.com/ajax/services/search/images?v=1.0&"
                        + "as_filetype=jpg&safe=moderate&imgsz=large&q="
                        + URLEncoder.encode(text, "UTF-8"));
                URLConnection connection = url.openConnection();

                String line;
                StringBuilder builder = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                JSONObject json = new JSONObject(builder.toString());
                String imageUrl = json.getJSONObject("responseData").getJSONArray("results").
                        getJSONObject(0).getString("url");

                URLConnection urlConnection = new URL(imageUrl).openConnection();
                urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3) " +
                        "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36");
                Image image = ImageIO.read(urlConnection.getInputStream());

                BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),
                        image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
                Graphics2D graphics2D = bufferedImage.createGraphics();
                graphics2D.drawImage(image, 0, 0, null);
                graphics2D.dispose();

                ImageIO.write(bufferedImage, "jpg", outputStream);

//                JOptionPane.showMessageDialog(null, "", "", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(image));
                return outputStream.toByteArray();
            }
        }

        // Works with original word
        private void setOriginal(String text) throws Exception {
            if (text.equals(""))
                throw new Exception("Empty string! Nya-ha-ha! :3 Why u so mad?");

            original.set(text);
            System.out.println("setOriginal: " + text);

            ImageThread imageThread = new ImageThread();

            // if no originalLink -> it's new Word
            // else it's just editing
            if (originalLink == null) {
                if (selectedGroup.getId() == 0)
                    throw new Exception("Trying to add word to abstract root group.. I CANT :(");

                Word newWord = new Word();
                newWord.setText(text);
                newWord.setLangId(settings.get(settings.LEFT_LANGUAGE).getValue());

                originalLink = newWord;

                // if it's first word to group
                if (selectedGroup.getWords() == null) {
                    selectedGroup.initWords();
                }
                selectedGroup.getWords().add(newWord);

                newWord.saveTo(selectedGroup);
            } else {
                originalLink.setText(text);
                originalLink.update();
            }
            // save picture
            imageThread.execute(originalLink);
        }

        public String getTranslation() {
            return translation.get();
        }

        // Works with translation
        public void setTranslation(String text) throws Exception {
            if (originalLink == null)
                throw new Exception("No original given. Need to set original first");
            if (text.equals(""))
                throw new Exception("Empty string! Nya-ha-ha! :3 Why u so mad?");

            translation.set(text);
            System.out.println("setTranslation: " + text);

            // if no translationLink -> it's new Translation
            // else it's just editing
            if (translationLink == null) {
                // if it's first translation of original
                if (originalLink.getTranslations() == null) {
                    originalLink.initTranslations();
                }
                Word newTranslation = new Word();
                translationLink = newTranslation;
                newTranslation.setText(text);
                newTranslation.setLangId(settings.get(settings.RIGHT_LANGUAGE).getValue());
                newTranslation.setPicture(null);
                originalLink.getTranslations().add(newTranslation);

                newTranslation.saveTo(originalLink, selectedGroup);
            } else {
                translationLink.setText(text);
                translationLink.update();
            }
        }

        public String getComment() {
            return comment.get();
        }

        // Works with your cat. LOL, joke :3 works with comment.
        public void setComment(String text) throws Exception {
            if (originalLink == null)
                throw new Exception("No original given. Need to set original first.");
            if (translationLink == null)
                throw new Exception("What are u commenting for? Translation please.");
            //if (text.equals(""))
            //   throw new Exception("Empty string! Nya-ha-ha! :3 Why u so mad?");

            comment.set(text);
            System.out.println("setComment: " + text);

            translationLink.setComment(text);
            translationLink.update();
        }

        public boolean isShowOriginal() {
            return showOriginal;
        }
        public void setShowOriginal(boolean showOriginal) {
            this.showOriginal = showOriginal;
        }
    }

    public class EditingCell extends TableCell<TableWord, String> {
        private TextField textField;
        // only for startEdit(), cancelEdit() and textField focus handler
        private String oldTextFieldValue;

        public EditingCell() {
            // display only graphics - only text field in cell
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

            textField = new TextField();

            // focus gained/lost handler
            textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
                // if focus lost -> cancel or commit edit
                // else focus gained -> start edit
                if (!newValue) {
                    if (oldTextFieldValue.equals(textField.getText())) {
                        cancelEdit();
                    } else {
                        try {
                            commitChanges(textField.getText());
                        } catch (Exception e) {
                            //TODO: display error information! display it!
                            e.printStackTrace();
//                            System.out.println(e.getMessage());
                            cancelEdit();
                        }
                    }
                } else {
                    startEdit();
                }
            });
            // pressed ESC -> cancel edit
            textField.setOnKeyPressed(event -> {
                if (event.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                }
            });
//            textField.setStyle("-fx-text-box-border: none; " +
//                    "-fx-background-color: transparent;");
        }

        @Override
        public void startEdit() {
            oldTextFieldValue = textField.getText();
        }

        @Override
        public void cancelEdit() {
            textField.setText(oldTextFieldValue);
        }

        public void commitChanges(String newValue) throws Exception {
            // 1st column -> edit Original
            // 2nd column -> edit Translation
            // 3rd column -> edit Comment
            if (currentColumnIndexIs(0)) {
                getCurrentRowItem().setOriginal(newValue);
            } else if (currentColumnIndexIs(1)) {
                getCurrentRowItem().setTranslation(newValue);

                // if it's last row in table -> insert new row
                if (getIndex() == getTableView().getItems().size() - 1) {
                    addEmptyRowItem();
                }
            } else if (currentColumnIndexIs(2)) {
                getCurrentRowItem().setComment(newValue);
            }

            // also enable context menu
            if (textField.getContextMenu() != null) {
                for (int menuItemI = 0;
                     menuItemI < textField.getContextMenu().getItems().size();
                     menuItemI++) {
                    textField.getContextMenu().getItems().get(menuItemI).setDisable(false);
                }
            }
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (!isEmpty()) {
                textField.setText(getItemText());

                // First table column and no content -> disable textField
                if (currentColumnIndexIs(0) && item == null) {
                    textField.setEditable(false);
                    textField.setFocusTraversable(false);
                }

                // Last empty row with TableWord -> enable textField for input
                if (getTableRow().getIndex() == getTableView().getItems().size() - 1) {
                    textField.setEditable(true);
                    textField.setFocusTraversable(true);
                }

                // First table column -> add context menu for Original
                // Else context menu for Translation/Comment
                if (currentColumnIndexIs(0)) {
                    MenuItem addTranslationItem = new MenuItem("Add new translation");
                    MenuItem removeWordItem = new MenuItem("Remove selected word with translations");
                    textField.setContextMenu(new ContextMenu(addTranslationItem, removeWordItem));

                    // Add new translation click -> insert empty row for input
                    addTranslationItem.setOnAction(event -> {
                        int rowIndex = getTableRow().getIndex();
                        while ((rowIndex + 1)  < getTableView().getItems().size() &&
                                !getRowItem(rowIndex + 1).isShowOriginal()) {
                            rowIndex++;
                        }
                        addEmptyTranslationRowItem(
                                rowIndex + 1,
                                getCurrentRowItem().getOriginalLink()
                        );
                    });
                    // Remove selected word -> delete rows from current row till other Original row
                    removeWordItem.setOnAction(event -> {
                        int rowIndex = getTableRow().getIndex();
                        while ((rowIndex + 1) < getTableView().getItems().size() &&
                                !getRowItem(rowIndex + 1).isShowOriginal()) {
                            rowIndex++;
                        }
                        if(!getCurrentRowItem().getTranslation().isEmpty()) {
                            // delete translation word
                            getCurrentRowItem().deleteTranslation();
                        }
                        // delete original word
                        getCurrentRowItem().deleteOriginal();
                        // delete items/rows from data/table
                        deleteRowItems(getTableRow().getIndex(), rowIndex + 1);
                        // focus trick :)
                        getTableView().requestFocus();
                    });

                    if (!getCurrentRowItem().isShowOriginal() || item == null) {
                        addTranslationItem.setDisable(true);
                        removeWordItem.setDisable(true);
                    }
                } else {
                    MenuItem removeTranslationItem = new MenuItem("Remove selected translation");
                    textField.setContextMenu(new ContextMenu(removeTranslationItem));

                    // Remove translation click -> delete row
                    removeTranslationItem.setOnAction(event -> {
                        // delete translation
                        getCurrentRowItem().deleteTranslation();
                        // delete item/row from data/table

                        if (!getCurrentRowItem().isShowOriginal()) {
                            deleteRowItem(getTableRow().getIndex());
                        }
                        // focus trick :)
                        getTableView().requestFocus();
                    });
                }
                setGraphic(textField);
            } else {
                setText(null);
                setGraphic(null);
            }
        }

        private void addEmptyRowItem() {
            getTableView().getItems().add(new TableWord(null, null, true));
        }
        private void deleteRowItem(int position) {
            if (position >= 0 && position < getTableView().getItems().size()) {
                getTableView().getItems().remove(position);
            } else {
                System.out.println("deleteRowItem error. Index out of boundary.");
            }
        }
        private void deleteRowItems(int from, int to) {
            if (from < 0 || from >= getTableView().getItems().size()) {
                System.out.println("deleteRowItems error. from: Index out of boundary.");
            } else if (to < 0 || to >= getTableView().getItems().size()) {
                System.out.println("deleteRowItems error. to: Index out of boundary.");
            } else {
                getTableView().getItems().remove(from, to);
            }
        }
        private void addEmptyTranslationRowItem(int position, Word originalLink) {
            if (position >= 0 && position < getTableView().getItems().size()) {
                if (originalLink != null) {
                    getTableView().getItems().add(position, new TableWord(originalLink, null, false));
                } else {
                    System.out.println("addEmptyTranslationRowItem error. Original link is null.");
                }
            } else {
                System.out.println("addEmptyTranslationRowItem error. Index out of boundary.");
            }
        }
        private TableWord getRowItem(int position) {
            if (position >= 0 && position < getTableView().getItems().size()) {
                return getTableView().getItems().get(position);
            } else {
                System.out.println("getRowItem error. Index out of boundary.");
            }
            return null;
        }
        private TableWord getCurrentRowItem() {
            return getRowItem(getTableRow().getIndex());
        }
        private boolean currentColumnIndexIs(int index) {
            if (index >= 0 && index < getTableView().getColumns().size()) {
                return getTableColumn() == getTableView().getColumns().get(index);
            } else {
                System.out.println("currentColumnIndexIs error. Index out of boundary.");
            }
            return false;
        }

        private String getItemText() {
            return getItem() == null ? "" : getItem();
        }

    }

    public class TreeEditingCell extends TreeCell<Group> {
        private CheckBox checkBox;
        private TextField textField;
        private String oldTextFieldValue;

        public TreeEditingCell() {
            this.checkBox = new CheckBox();
            this.checkBox.setOnAction(event1 -> {
                if (this.checkBox.isSelected()) {
                    SQLiteDB.insert(getItem());
                } else {
                    SQLiteDB.deleteCurrentlySelectedGroup(getItem());
                }
            });

            textField = new TextField();
            textField.setOnKeyPressed(event -> {
                if(event.getCode().equals(KeyCode.ENTER)) {
                    commitChanges(textField.getText());
                }
            });
            textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if(!newValue) {
                    commitChanges(textField.getText());
                }
            });

            MenuItem addSubgroupItem = new MenuItem("Add subgroup");
            MenuItem editItem = new MenuItem("Edit");
            MenuItem deleteItem = new MenuItem("Delete");

            addSubgroupItem.setOnAction(event -> {
                getTreeItem().setExpanded(true);
                Group group = new Group();
                group.setName("New group");
                group.saveTo(getItem(), leftLangChoiceBox.getValue());

                if (getItem().getSubGroups() == null) {
                    getItem().initSubGroups();
                }

                getItem().getSubGroups().add(group);
                getTreeItem().getChildren().add(new TreeItem<>(group));
            });
            editItem.setOnAction(event -> startEdit());
            deleteItem.setOnAction(event -> {
                getItem().delete();
                if(getTreeItem().getValue().getId() != 0) {
                    getTreeItem().getParent().getChildren().remove(getTreeItem());
                } else {
                    for(int j = 0; j < groupsTreeView.getSelectionModel().getSelectedItems().
                            get(0).getChildren().size() + 3; j++) {

                        for (int i = 1; i < groupsTreeView.getSelectionModel().getSelectedItems().
                                get(0).getChildren().size(); i++) {

                            getTreeItem().getChildren().remove(getTreeView().getSelectionModel().
                                    getSelectedItems().get(0).getChildren().get(i));
                        }
                    }
                }
            });
            this.setContextMenu(new ContextMenu(addSubgroupItem, editItem, deleteItem));
        }

        @Override
        public void updateItem(Group item, boolean empty) {
            super.updateItem(item, empty);
            if (!isEmpty()) {
                if(currentlySelectedGroups.contains(item.getId())) {
                    this.checkBox.setSelected(true);
                } else {
                    this.checkBox.setSelected(false);
                }

                if(item.getId() == 1) {
                    disableAllElementsContextMenu(true);
                } else {
                    disableAllElementsContextMenu(false);
                }

                if(item.getId() == 0) {
                    this.getContextMenu().getItems().get(1).setDisable(true);
                }

                setText(item.getName());
                setGraphic(checkBox);
                this.setOnMouseClicked(event -> {
                    // left mouse click
                    if (event.getButton().equals(MouseButton.PRIMARY)) {
                        data.clear();
                        if(item.getId() == 0) {
                            recursiveMakeData(item);
                        } else {
                            makeData(item);
                        }
                        selectedGroup = item;
                        data.add(new TableWord(null, null, true));
                    }
                });
            } else {
                setText(null);
                setGraphic(null);
            }
        }

        @Override
        public void startEdit() {
            if(getItem().getId() == 0) {
                return;
            }
            super.startEdit();
            textField.setText(getItem().getName());
            oldTextFieldValue = textField.getText();
            setText(null);
            setGraphic(textField);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            setText(getItem().getName());
            setGraphic(checkBox);
        }

        public void commitChanges(String newValue) {
            super.commitEdit(getItem());
            if(!newValue.isEmpty()) {
                getItem().setName(newValue);
                getItem().update(leftLangChoiceBox.getValue());
                setText(newValue);
            } else {
                setText(oldTextFieldValue);
            }
        }

        private void disableAllElementsContextMenu(boolean disable) {
            for(int i = 0; i < this.getContextMenu().getItems().size(); i++) {
                this.getContextMenu().getItems().get(i).setDisable(disable);
            }
        }

        public CheckBox getCheckBox() {
            return checkBox;
        }
    }

    public void setSettings(Settings settings) {
        this.settings = settings;

        currentlySelectedGroups = SQLiteDB.getCurrentlySelectedGroups();
        // build data
        buildData();
        // Languages
        initLanguageBoxes();
    }

    public ArrayList getData() {
        return (ArrayList) data;
    }
}
