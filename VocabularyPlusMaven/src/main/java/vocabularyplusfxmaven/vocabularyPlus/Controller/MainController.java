package vocabularyplusfxmaven.vocabularyPlus.Controller;

import javafx.scene.Node;
import vocabularyplusfxmaven.vocabularyPlus.Model.*;
import vocabularyplusfxmaven.vocabularyPlus.SQLiteDB;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Vyacheslav on 2/13/2015.
 * This is main window of app.
 */
public class MainController implements Initializable {
    public Settings settings;
    public ObservableList<Language> languageCollection;
    public Group group;
    private int translationGridRows;
    private int translationGridColumns;
    private ArrayList<Word> words;
    private ArrayList<Word> wordsOnButtons;
    private ArrayList<Integer> currentlySelectedGroups;
    private ArrayList<Integer> intervals;
    private HashMap<Integer, WordShow> wordShowCollection;
    private Queue<Word> outputWords;
    private Stage mainStage;
    private MainController mainController;
    boolean isTranslation, isSettings;
    @FXML private ToolBar toolBar;
    @FXML private Button translationsButton;
    @FXML private Button settingsButton;
    @FXML private GridPane translationsContainer;

    @FXML
    public void initialize(URL location, ResourceBundle resourceBundle) {
        mainController = this;
//        SQLiteDB.deleteAllFromWord();
        wordsOnButtons = new ArrayList<>();
        words = new ArrayList<>();
        settings = SQLiteDB.getSettings();
        languageCollection = SQLiteDB.getLanguages();
        group = SQLiteDB.getGroups(settings);
        wordShowCollection = SQLiteDB.getWordsShow();
        intervals = SQLiteDB.getWordShowIntervals();
        translationGridRows = settings.get(settings.ROWS_COUNT).getValue();
        translationGridColumns = settings.get(settings.COLS_COUNT).getValue();
        currentlySelectedGroups = SQLiteDB.getCurrentlySelectedGroups();


        recursiveGroupGetWords(group, currentlySelectedGroups);

        fillTranslationGrid();
        tempfillWordShowCollection();

        bindViewActions();

        for (Node wordButton : translationsContainer.getChildren()) {
            ((WordButton) wordButton).setPrefSize(
                    settings.get(settings.CELL_SIZE).getValue(),
                    settings.get(settings.CELL_SIZE).getValue()
            );
        }

        if(!words.isEmpty()) {
            startTimers();
        }
//        displayWOrdShowCollection();
    }

    private void startTimers() {
        // set timers
        outputWords = new LinkedList<>();
        ChangerWords changerWords = new ChangerWords();
        Timer counterTimer = new Timer();
        Timer changerWordsTimer = new Timer();
        Counter counter = new Counter(outputWords);

        fillPassedWords();

        counterTimer.schedule(counter, 0, 1000);
        changerWordsTimer.schedule(changerWords, 0, 120000);
    }

    private class Counter extends TimerTask {
        private Queue<Word> outputWords;

        public Counter(Queue<Word> outputWords) {
            this.outputWords = outputWords;
        }

        @Override
        public void run() {
            fillOutputWords(outputWords);
        }
    }

    private class ChangerWords extends TimerTask {

        public ChangerWords() {}

        @Override
        public void run() {
            Platform.runLater(() -> {
                try {
                    fillButtons();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private void fillPassedWords() {
        System.out.println("fillPassedWords");
        long switchedOffTime = SQLiteDB.getSwitchedOffTime();

        for(Word word : words) {
            int index = word.getId();
            long wordWillShown = wordShowCollection.get(index).getShowedTime() +
                    intervals.get(wordShowCollection.get(index).getShowCount());

            if(wordWillShown > switchedOffTime &&
                    wordWillShown < millisecondToMinutes(System.currentTimeMillis())) {

                outputWords.add(word);
                System.out.println("fillPassedWords Word := " + word.getText());
            }
        }
    }

    private void fillOutputWords(Queue<Word> outputWords) {
        if(wordShowCollection == null) {
            return;
        }

        boolean wordsReadyToDisplay = false;
        long currentTime = millisecondToMinutes(System.currentTimeMillis());
        // if there are words which need to display at current time
        for (Word word1 : words) {
            int index = word1.getId();
            if (wordShowCollection.get(index).getShowCount() < intervals.size()) {

                if (currentTime == wordShowCollection.get(index).getShowedTime() +
                        intervals.get(wordShowCollection.get(index).getShowCount())) {
                    wordsReadyToDisplay = true;
                    break;
                }
            }
        }

        if (wordsReadyToDisplay) {
            for (Word word : words) {
                int index = word.getId();
                if (wordShowCollection.get(index).getShowCount() < intervals.size()) {
                    if (currentTime == wordShowCollection.get(index).getShowedTime() +
                            intervals.get(wordShowCollection.get(index).getShowCount())) {
                        if(!outputWords.contains(word)) {
                            outputWords.add(word);
                        }
                    }
                }
            }
        } else {
            // checking are there new words in outputWords.
            // there are enough new words -> return
            // there are not enough new words -> add new words
            if(wordShowCollection == null) {
                return;
            }
            for (int i = 0, count = 0; i < outputWords.size(); i++) {
                int index = outputWords.element().getId();
                if (wordShowCollection.get(index).getShowCount() == 0) {
                    count++;

                    if (count > (translationGridRows * translationGridColumns)) {
                        return;
                    }
                }
            }

            // add new words
            for (int i = 0, count = 0; i < words.size(); i++) {
                if (count >= translationGridRows * translationGridColumns) {
                    break;
                }
                int index = words.get(i).getId();
                if (wordShowCollection.get(index).getShowCount() == 0) {
                    if(!outputWords.contains(words.get(i))) {
                        outputWords.add(words.get(i));
                        count++;
                    }
                }
            }
        }
    }

    private void fillButtons() throws Exception {
        if(translationsContainer.getChildren().get(0) instanceof Label) {
            return;
        }
        if (!outputWords.isEmpty()) {
            saveWordShow();
            wordsOnButtons.clear();
            for (int i = 0; i < translationGridRows *  translationGridColumns; i++) {
                Word word = outputWords.element();
                System.out.print("time = " + millisecondToMinutes(System.currentTimeMillis()) + " : ");
                System.out.println(word.getId() + " : " + word.getText() + " : wordId = " +
                        wordShowCollection.get(word.getId()).getWordId() + " : showCount = " +
                        wordShowCollection.get(word.getId()).getShowCount() + " : showedTime = " +
                        wordShowCollection.get(word.getId()).getShowedTime());
                WordButton button = (WordButton) translationsContainer.getChildren().get(i);
                button.setWordLink(word);
                button.setText(button.getWordLink().getText());
                button.setBackground(button.getImage());

                outputWords.remove();

                wordsOnButtons.add(button.getWordLink());
            }
            System.out.println("*****************************************************************");
        }
    }

    private void fillButtonsByWordsOnButtons() throws Exception {
        if(translationsContainer.getChildren().get(0) instanceof Label) {
            return;
        }
        if (!wordsOnButtons.isEmpty()) {
            saveWordShow();

            for (int i = 0; i < translationGridRows * translationGridColumns; i++) {
                if(i >= wordsOnButtons.size()) {
                    break;
                }
                Word word = wordsOnButtons.get(i);
                WordButton button = (WordButton) translationsContainer.getChildren().get(i);
                button.setWordLink(word);
                button.setText(button.getWordLink().getText());
                button.setBackground(button.getImage());
            }
        }
    }

    private void fillButtonsRandom() throws IOException {
        Random random = new Random();
        // if on all buttons all words
        if(wordsOnButtons.containsAll(words)) {
            return;
        }

        ArrayList<Word> tempWordsOnButtons = new ArrayList<>();
        ArrayList<Integer> tempIndexes = new ArrayList<>();

        if(words.size() >= (translationGridRows * translationGridColumns) * 2) {
            // output unique words
            for (int i = 0; i < translationGridRows * translationGridColumns; i++) {
                WordButton button = (WordButton) translationsContainer.getChildren().get(i);
                boolean fl = true;
                while (fl) {
                    int index = random.nextInt(words.size());
                    // if there are not words on the buttons and index is unique
                    if (!wordsOnButtons.contains(words.get(index)) && !tempIndexes.contains(index)) {
                        fl = false;

                        button.setWordLink(words.get(index));
                        button.setText(button.getWordLink().getText());
                        button.setBackground(button.getImage());

                        tempWordsOnButtons.add(words.get(index));
                        tempIndexes.add(index);
                    }
                }
            }
        } else {
            // output not unique words
            for (int i = 0; i < translationGridRows * translationGridColumns; i++) {
                WordButton button = (WordButton) translationsContainer.getChildren().get(i);
                boolean fl = true;
                while (fl) {
                    int index = random.nextInt(words.size());
                    if (!wordsOnButtons.contains(words.get(index))) {
                        fl = false;

                        button.setWordLink(words.get(index));
                        button.setText(button.getWordLink().getText());

                        tempWordsOnButtons.add(words.get(index));
                    }
                }
            }
        }

        wordsOnButtons.clear();
        wordsOnButtons.addAll(tempWordsOnButtons);

    }

    // savePicture WordShow in DB
    private void saveWordShow() throws Exception {
        if(wordShowCollection == null) {
            return;
        }
        ArrayList<Integer> uniqueIds = new ArrayList<>();

        for (int i = 0; i < translationGridRows * translationGridColumns; i++) {

            WordButton button = (WordButton) translationsContainer.getChildren().get(i);

            if (button.getWordLink() != null) {
                if (!uniqueIds.contains(button.getWordLink().getId())) {
                    uniqueIds.add(button.getWordLink().getId());
                    if(wordShowCollection.containsKey(button.getWordLink().getId())) {
                        WordShow wordShow = wordShowCollection.get(button.getWordLink().getId());
                        wordShow.setShowedTime(millisecondToMinutes(System.currentTimeMillis()));
                        wordShow.setShowCount(wordShow.getShowCount() + 1);
                        wordShow.update();
                    }
                }
            }
        }
        uniqueIds.clear();
    }

    private void fillTranslationGrid() {
        if(!words.isEmpty()) {
            setConstraintsTranslationContainer(translationGridColumns, translationGridRows);

            for (int row = 0; row < translationGridRows; row++) {
                for (int column = 0; column < translationGridColumns; column++) {

                    WordButton button = new WordButton();
                    translationsContainer.add(button, column, row);
                }
            }
        } else {
            setLabelTranslationsContainer();
        }
    }

    private void setConstraintsTranslationContainer(Integer columnCount, Integer rowCount) {
        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setPercentWidth(100.0 / columnCount);
        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setPercentHeight(100.0 / rowCount);

        for (int row = 0; row < rowCount; row++)
            translationsContainer.getRowConstraints().add(row, rowConstraints);
        for (int column = 0; column < columnCount; column++)
            translationsContainer.getColumnConstraints().add(column, columnConstraints);
    }

    private void clearTranslationContainer() {
        translationsContainer.getChildren().clear();
        translationsContainer.getRowConstraints().clear();
        translationsContainer.getColumnConstraints().clear();
    }

    private void recursiveGroupGetWords(Group group, ArrayList<Integer> selectedGroupId) {
        if(group.getSubGroups() != null) {
            for(int i = 0; i < group.getSubGroups().size(); i++) {
                if(selectedGroupId.contains(group.getId())) {
                    if (group.getWords() != null) {
                        for (int j = 0; j < group.getWords().size(); j++) {
                            words.add(group.getWords().get(j));
                        }
                    }
                }
                Group subGroup = group.getSubGroups().get(i);
                recursiveGroupGetWords(subGroup, selectedGroupId);
            }
        } else {
            if(selectedGroupId.contains(group.getId())) {
                if (group.getWords() != null) {
                    for (int j = 0; j < group.getWords().size(); j++) {
                        words.add(group.getWords().get(j));
                    }
                }
            }
        }
    }

    public long millisecondToMinutes(long milliseconds) {
        return TimeUnit.MILLISECONDS.toMinutes(milliseconds);
    }

    private void bindViewActions() {

        /* opens Translations window */
        translationsButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(isTranslation) {
                    return;
                }
                try {

                    FXMLLoader fxmlLoader = new
                            FXMLLoader(getClass().getResource("/UI/TranslationView.fxml"));
                    Parent root = fxmlLoader.load();
                    TranslationController translationController = fxmlLoader.getController();

                    translationController.setSettings(settings);

                    Stage stage = new Stage();
                    stage.setTitle("Translations");
                    stage.initStyle(StageStyle.UTILITY);
                    stage.initModality(Modality.WINDOW_MODAL);
                    stage.setScene(new Scene(root));
                    stage.show();

                    stage.setOnCloseRequest(event1 -> {
                        try {
                            isTranslation = false;

                            Object o = words.clone();
                            ArrayList tempWords = ((ArrayList) o);
                            words.clear();
                            group = SQLiteDB.getGroups(settings);
                            wordShowCollection = SQLiteDB.getWordsShow();
                            currentlySelectedGroups = SQLiteDB.getCurrentlySelectedGroups();

                            recursiveGroupGetWords(group, currentlySelectedGroups);

                            if (!tempWords.isEmpty()) {
                                if (words.isEmpty()) {
                                    setLabelTranslationsContainer();
                                } else {
                                    ArrayList<Word> temps = new ArrayList<>();
                                    temps.addAll(outputWords);
                                    outputWords.clear();

                                    for(Word word : words) {
                                        for (Word temp : temps) {
                                            if (Objects.equals(temp.getId(), word.getId())) {
                                                outputWords.add(word);
                                            }
                                        }
                                    }
                                    fillButtons();
                                }
                            } else {
                                startTimers();
                                clearTranslationContainer();
                                fillTranslationGrid();
                            }
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    });
                    isTranslation = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        /* opens Settings window */
        settingsButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(isSettings) {
                    return;
                }
                try {
                    FXMLLoader fxmlLoader = new
                            FXMLLoader(getClass().getClassLoader().getResource("UI/SettingsView.fxml"));
                    Parent root = fxmlLoader.load();
                    SettingsController settingsController = fxmlLoader.getController();
                    settingsController.setSettings(settings);
                    settingsController.setMainStage(mainStage);
                    settingsController.setMainController(mainController);
                    settingsController.setWordButtons(translationsContainer.getChildren());

                    Stage stage = new Stage();
                    stage.setWidth(275);
                    stage.setHeight(230);
                    stage.setTitle("Settings");
                    stage.initStyle(StageStyle.UTILITY);
                    stage.initModality(Modality.WINDOW_MODAL);
                    stage.setResizable(false);
                    stage.setScene(new Scene(root));
                    stage.show();

                    stage.setOnCloseRequest(event1 -> {
                        try {
                            isSettings = false;
                            if (translationGridColumns == settings.get(settings.COLS_COUNT).getValue() &&
                                    translationGridRows == settings.get(settings.ROWS_COUNT).getValue()
                                    ) {
                                return;
                            }

                            settings = SQLiteDB.getSettings();
                            translationGridRows = settings.get(settings.ROWS_COUNT).getValue();
                            translationGridColumns = settings.get(settings.COLS_COUNT).getValue();
                            clearTranslationContainer();
                            fillTranslationGrid();
                            fillButtonsByWordsOnButtons();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    });
                    isSettings = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void updateCellsMainWindow() {
        try {
            settings = SQLiteDB.getSettings();
            translationGridRows = settings.get(settings.ROWS_COUNT).getValue();
            translationGridColumns = settings.get(settings.COLS_COUNT).getValue();
            clearTranslationContainer();
            fillTranslationGrid();
            fillButtonsByWordsOnButtons();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteCurrentWordButton() throws IOException {
        if(translationsContainer.getChildren().get(0) instanceof WordButton) {
            for (int i = 0; i < translationGridRows * translationGridColumns; i++) {
                WordButton button = (WordButton) translationsContainer.getChildren().get(i);
                if (!words.contains(button.getWordLink())
                        && !wordShowCollection.containsKey(button.getWordLink().getId())) {
                    button.setWordLink(null);
                    button.setText(null);
                    button.setBackground(button.setBackground());
                }
            }
        }
    }

    private void setLabelTranslationsContainer() {
        clearTranslationContainer();
        setConstraintsTranslationContainer(1, 1);

        Label label = new Label("Your words appear here.");
        label.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        label.setAlignment(Pos.CENTER);
        translationsContainer.add(label, 0, 0);
    }

    public Stage getMainStage() {
        return mainStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;

    }

    private void fakeData() {
        /*********************************************************************/
        Group group1 = new Group(1, "group1");
        group1.initWords();
        //group1.initSubGroups();

        Group group2 = new Group(2, "group2");
        group2.initSubGroups();
        group2.initWords();
        //group1.getSubGroups().add(group2);

        Group group3 = new Group(3, "group3");
        group3.initSubGroups();
        group3.initWords();
        group2.getSubGroups().add(group3);

        Group group4 = new Group(4, "group4");
        group4.initSubGroups();
        group4.initWords();
        group3.getSubGroups().add(group4);

        Group group5 = new Group(5, "group5");
        group5.initWords();
        group4.getSubGroups().add(group5);

/*        group1.saveTo(group1);
        group2.saveTo(group1);
        group3.saveTo(group2);
        group4.saveTo(group3);
        group5.saveTo(group4);*/
        /*********************************************************************/
//        Word word1 = new Word(1, "word1", "com1", "picture1", 1);
//        word1.initTranslations();
//        Word word11 = new Word(2, "word11", "com11", "picture11", 2);
//        Word word111 = new Word(3, "word111", "com111", "picture111", 2);
//        word1.getTranslations().add(word11);
//        word1.getTranslations().add(word111);
//        /*********************************************************************/
//        Word word2 = new Word(4, "word2", "com2", "picture2", 1);
//        word2.initTranslations();
//        Word word22 = new Word(5, "word22", "com22", "picture22", 2);
//        word2.getTranslations().add(word22);
//        /*********************************************************************/
//        Word word3 = new Word(6, "word3", "com3", "picture3", 1);
//        word3.initTranslations();
//        Word word33 = new Word(7, "word33", "com33", "picture33", 2);
//        word3.getTranslations().add(word33);
//        /*********************************************************************/
//        Word word4 = new Word(8, "word4", "com4", "picture4", 1);
//        word4.initTranslations();
//        Word word44 = new Word(9, "word44", "com44", "picture44", 2);
//        word4.getTranslations().add(word44);
//        /*********************************************************************/
//        Word word5 = new Word(10, "word5", "com5", "picture5", 1);
//        word5.initTranslations();
//        Word word55 = new Word(11, "word55", "com55", "picture55", 2);
//        Word word555 = new Word(12, "word555", "com555", "picture555", 2);
//        Word word5555 = new Word(13, "word5555", "com5555", "picture5555", 2);
//        word5.getTranslations().add(word55);
//        word5.getTranslations().add(word555);
//        word5.getTranslations().add(word5555);
//        /*********************************************************************/
//        Word word6 = new Word(14, "word6", "com6", "picture6", 1);
//        word6.initTranslations();
//        Word word66 = new Word(15, "word66", "com66", "picture66", 2);
//        word6.getTranslations().add(word66);
//        /*********************************************************************/
//        Word word7 = new Word(16, "word7", "com7", "picture7", 1);
//        word7.initTranslations();
//        Word word77 = new Word(17, "word77", "com77", "picture77", 2);
//        word7.getTranslations().add(word77);
//        /*********************************************************************/
//        Word word8 = new Word(18, "word8", "com8", "picture8", 1);
//        word8.initTranslations();
//        Word word88 = new Word(19, "word88", "com88", "picture88", 2);
//        Word word888 = new Word(20, "word888", "com888", "picture888", 2);
//        Word word8888 = new Word(21, "word8888", "com8888", "picture8888", 2);
//        word8.getTranslations().add(word88);
//        word8.getTranslations().add(word888);
//        word8.getTranslations().add(word8888);
//        /*********************************************************************/
//        Word word9 = new Word(22, "word9", "com9", "picture9", 1);
//        word9.initTranslations();
//        Word word99 = new Word(23, "word99", "com99", "picture99", 2);
//        word9.getTranslations().add(word99);
//        /*********************************************************************/
//        Word word10 = new Word(24, "word10", "com10", "picture10", 1);
//        word10.initTranslations();
//        Word word1010 = new Word(25, "word1010", "com1010", "picture1010", 2);
//        word10.getTranslations().add(word1010);
        /*********************************************************************/
//        Word word1 = new Word(null, "word1", "com1", "picture1", 1);
//        word1.initTranslations();
//        Word word11 = new Word(null, "word11", "com11", "picture11", 2);
//        Word word111 = new Word(null, "word111", "com111", "picture111", 2);
//        word1.getTranslations().add(word11);
//        word1.getTranslations().add(word111);
//        /*********************************************************************/
//        Word word2 = new Word(null, "word2", "com2", "picture2", 1);
//        word2.initTranslations();
//        Word word22 = new Word(null, "word22", "com22", "picture22", 2);
//        word2.getTranslations().add(word22);
//        /*********************************************************************/
//        Word word3 = new Word(null, "word3", "com3", "picture3", 1);
//        word3.initTranslations();
//        Word word33 = new Word(null, "word33", "com33", "picture33", 2);
//        word3.getTranslations().add(word33);
//        /*********************************************************************/
//        Word word4 = new Word(null, "word4", "com4", "picture4", 1);
//        word4.initTranslations();
//        Word word44 = new Word(null, "word44", "com44", "picture44", 2);
//        word4.getTranslations().add(word44);
//        /*********************************************************************/
//        Word word5 = new Word(null, "word5", "com5", "picture5", 1);
//        word5.initTranslations();
//        Word word55 = new Word(null, "word55", "com55", "picture55", 2);
//        Word word555 = new Word(null, "word555", "com555", "picture555", 2);
//        Word word5555 = new Word(null, "word5555", "com5555", "picture5555", 2);
//        word5.getTranslations().add(word55);
//        word5.getTranslations().add(word555);
//        word5.getTranslations().add(word5555);
//        /*********************************************************************/
//        Word word6 = new Word(null, "word6", "com6", "picture6", 1);
//        word6.initTranslations();
//        Word word66 = new Word(null, "word66", "com66", "picture66", 2);
//        word6.getTranslations().add(word66);
//        /*********************************************************************/
//        Word word7 = new Word(null, "word7", "com7", "picture7", 1);
//        word7.initTranslations();
//        Word word77 = new Word(null, "word77", "com77", "picture77", 2);
//        word7.getTranslations().add(word77);
//        /*********************************************************************/
//        Word word8 = new Word(null, "word8", "com8", "picture8", 1);
//        word8.initTranslations();
//        Word word88 = new Word(null, "word88", "com88", "picture88", 2);
//        Word word888 = new Word(null, "word888", "com888", "picture888", 2);
//        Word word8888 = new Word(null, "word8888", "com8888", "picture8888", 2);
//        word8.getTranslations().add(word88);
//        word8.getTranslations().add(word888);
//        word8.getTranslations().add(word8888);
//        /*********************************************************************/
//        Word word9 = new Word(null, "word9", "com9", "picture9", 1);
//        word9.initTranslations();
//        Word word99 = new Word(null, "word99", "com99", "picture99", 2);
//        word9.getTranslations().add(word99);
//        /*********************************************************************/
//        Word word10 = new Word(null, "word10", "com10", "picture10", 1);
//        word10.initTranslations();
//        Word word1010 = new Word(null, "word1010", "com1010", "picture1010", 2);
//        word10.getTranslations().add(word1010);
//        /*********************************************************************/
//
//        group1.getWords().add(word1);
//        group2.getWords().add(word2);
//        group2.getWords().add(word8);
//        group2.getWords().add(word10);
//        group3.getWords().add(word3);
//        group3.getWords().add(word6);
//        group4.getWords().add(word4);
//        group4.getWords().add(word7);
//        group5.getWords().add(word5);
//        group5.getWords().add(word9);
/*        group = new Group(0, "root");
        group.initSubGroups();
        group.getSubGroups().add(group1);
        group.getSubGroups().add(group2);*/

//        word1.saveTo(group1);
//        word11.saveTo(word1, group1);
//        word111.saveTo(word1, group1);
//
//        word2.saveTo(group2);
//        word22.saveTo(word2, group2);
//
//        word3.saveTo(group3);
//        word33.saveTo(word3, group3);
//
//        word4.saveTo(group4);
//        word44.saveTo(word4, group4);
//
//        word5.saveTo(group5);
//        word55.saveTo(word5, group5);
//        word555.saveTo(word5, group5);
//        word5555.saveTo(word5, group5);
//
//        word6.saveTo(group3);
//        word66.saveTo(word6, group3);
//
//        word7.saveTo(group2);
//        word77.saveTo(word7, group2);
//
//        word8.saveTo(group2);
//        word88.saveTo(word8, group2);
//        word888.saveTo(word8, group2);
//        word8888.saveTo(word8, group2);
//
//        word9.saveTo(group2);
//        word99.saveTo(word9, group2);
//
//        word10.saveTo(group2);
//        word1010.saveTo(word10, group2);

//        word3.update();
//        word8.deleteFrom(group2);
//        word22.deleteFrom(word2, group2);
    }

    private void tempfillWordShowCollection() {
        for (Word word : words) {
            int index = word.getId();
            if (wordShowCollection.containsKey(index)) {
                wordShowCollection.get(index).setShowedTime(0);
                wordShowCollection.get(index).setShowCount(0);
                wordShowCollection.get(index).update();
            }
        }
    }

    private void displayWOrdShowCollection() {
        for (Word word : words) {
            if(wordShowCollection.containsKey(word.getId())) {

                WordShow wordShow = wordShowCollection.get(word.getId());

                System.out.println(
                        wordShow.getWordId() + " : " +
                                wordShow.getShowedTime() + " : " +
                                wordShow.getShowCount()
                );
            }
        }
        System.out.println("************");
    }
}
