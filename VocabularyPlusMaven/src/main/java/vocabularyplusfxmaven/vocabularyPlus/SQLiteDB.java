package vocabularyplusfxmaven.vocabularyPlus;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import vocabularyplusfxmaven.vocabularyPlus.Model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Vladimir on 05.10.2014.
 * This is class realize work with database SQLite
 */
public class SQLiteDB {
    private static Connection connection = null;

    /*
     * CONNECTION DATABASE METHODS
     */

    private static Connection getConnection() {
        if(connection == null) {
            try {
                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection("jdbc:sqlite:vocabulary.db");
                foreignKeyEnable();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    private static boolean foreignKeyEnable() {
        String query = "PRAGMA foreign_keys = ON;";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean deleteAllFromWord() {
        String query = "DELETE FROM `vp_word`";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /*
     * SETTINGS DATABASE METHODS
     */

    public static boolean update(Setting setting) {
        String query = "UPDATE `vp_settings` SET value = ? WHERE short_name = ?";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setInt(1, setting.getValue());
            statement.setString(2, setting.getShortName());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /*
     * LANGUAGE DATABASE METHODS
     */

    public static boolean insert(Language language) {
        String query = "INSERT INTO `vp_language` (short_name, name) VALUES(?, ?)";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setString(1, language.getShortName());
            statement.setString(2, language.getName());
            statement.executeUpdate();

            // set Id for current Language
            language.setId(statement.getGeneratedKeys().getInt(1));
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean update(Language language) {
        String query = "UPDATE `vp_language` SET short_name = ?, name = ? WHERE id = ?";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setString(1, language.getShortName());
            statement.setString(2, language.getName());
            statement.setInt(3, language.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean delete(Language language) {
        String query = "DELETE FROM `vp_language` WHERE id = ?";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setInt(1, language.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /*
     * GROUP DATABASE METHODS
     */

    public static boolean insert(Group group, Group rootGroup, Language language) {
        String groupQuery = "INSERT INTO `vp_group` (root_group) VALUES(?);";
        String groupNameQuery = "INSERT INTO `vp_group_name` (text, lang_id, group_id) " +
                "VALUES(?, ?, ?);";
        PreparedStatement groupStatement, groupNameStatement;
        try {
            getConnection().setAutoCommit(false);
            groupStatement      = getConnection().prepareStatement(groupQuery);
            groupNameStatement  = getConnection().prepareStatement(groupNameQuery);

            groupStatement.setInt(1, rootGroup.getId());
            groupStatement.executeUpdate();

            groupNameStatement.setString(1, group.getName());
            groupNameStatement.setInt(2, language.getId());
            groupNameStatement.setInt(3, groupStatement.getGeneratedKeys().getInt(1));
            groupNameStatement.executeUpdate();

            getConnection().commit();

            // set Id for current Group
            group.setId(groupStatement.getGeneratedKeys().getInt(1));
            getConnection().setAutoCommit(true);
        } catch(SQLException e) {
            if(getConnection() != null) {
                try {
                    e.printStackTrace();
                    getConnection().rollback();
                } catch(SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }
        return true;
    }

    public static boolean update(Group group, Language language) {
        String query = "UPDATE `vp_group_name` SET text = ? WHERE lang_id = ? AND group_id = ?";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setString(1, group.getName());
            statement.setInt(2, language.getId());
            statement.setInt(3, group.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean delete(Group group) {
        String query = "DELETE FROM `vp_group` WHERE id IN (" + group.getSubGroupsId() + ");";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /*
     * CURRENTLY SELECTED GROUPS DATABASE METHODS
     */

    public static boolean insert(Group group) {
        String query = "INSERT INTO `vp_currently_selected_groups` (group_id) VALUES (?)";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setInt(1, group.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean deleteCurrentlySelectedGroup(Group group) {
        String query = "DELETE FROM `vp_currently_selected_groups` WHERE group_id = ?";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setInt(1, group.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /*
     * WORD DATABASE METHODS
     */

    public static boolean insert(Word word, Group group) {
        String wordQuery = "INSERT INTO `vp_word` (text, lang_id, comment) " +
                "VALUES (?, ?, ?);";
        String groupWordQuery = "INSERT INTO `vp_group_word` (group_id, word_id) VALUES (?, ?);";
        String wordShowQuery = "INSERT INTO `vp_word_show` (word_id) VALUES (?);";
        PreparedStatement wordStatement, groupWordStatement, wordShowStatement;
        try {
            getConnection().setAutoCommit(false);
            wordStatement      = getConnection().prepareStatement(wordQuery);
            groupWordStatement = getConnection().prepareStatement(groupWordQuery);
            wordShowStatement  = getConnection().prepareStatement(wordShowQuery);

            wordStatement.setString(1, word.getText());
            wordStatement.setInt(2, word.getLangId());
            wordStatement.setString(3, word.getComment());
            wordStatement.executeUpdate();

            // set Id for current Word
            word.setId(wordStatement.getGeneratedKeys().getInt(1));

            groupWordStatement.setInt(1, group.getId());
            groupWordStatement.setInt(2, word.getId());
            groupWordStatement.executeUpdate();

            wordShowStatement.setInt(1, word.getId());
            wordShowStatement.executeUpdate();

            getConnection().commit();
            getConnection().setAutoCommit(true);
        } catch(SQLException e) {
            if (getConnection() != null) {
                try {
                    e.printStackTrace();
                    getConnection().rollback();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }
        return  true;
    }

    // save translation for original and save in group
    public static boolean insert(Word translation, Word original, Group group) {
        String translationQuery = "INSERT INTO `vp_word` (text, lang_id, comment) " +
                "VALUES (?, ?, ?);";
        String originalQuery = "INSERT INTO `vp_relation_word` (left_word_id, right_word_id) " +
                "VALUES (?, ?);";
        String groupWordQuery = "INSERT INTO `vp_group_word` (group_id, word_id) VALUES (?, ?);";
        PreparedStatement translationStatement, originalStatement, groupWordStatement;
        try {
            getConnection().setAutoCommit(false);
            translationStatement = getConnection().prepareStatement(translationQuery);
            originalStatement    = getConnection().prepareStatement(originalQuery);
            groupWordStatement   = getConnection().prepareStatement(groupWordQuery);

            translationStatement.setString(1, translation.getText());
            translationStatement.setInt(2, translation.getLangId());
            translationStatement.setString(3, translation.getComment());
            translationStatement.executeUpdate();

            // set Id for current translation
            translation.setId(translationStatement.getGeneratedKeys().getInt(1));

            originalStatement.setInt(1, original.getId());
            originalStatement.setInt(2, translation.getId());
            originalStatement.executeUpdate();

            groupWordStatement.setInt(1, group.getId());
            groupWordStatement.setInt(2, translation.getId());
            groupWordStatement.executeUpdate();

            getConnection().commit();
            getConnection().setAutoCommit(true);
        } catch(SQLException e) {
            if (getConnection() != null) {
                try {
                    e.printStackTrace();
                    getConnection().rollback();
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
            }
        }
        return  true;
    }

    public static boolean insertPicture(Word word) {
        String query = "UPDATE `vp_word` SET picture = ? WHERE id = ?;";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setBytes(1, word.getPicture());
            statement.setInt(2, word.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean update(Word word) {
        String query = "UPDATE `vp_word` SET text = ?, comment = ? WHERE id = ?;";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setString(1, word.getText());
            statement.setString(2, word.getComment());
            statement.setInt(3, word.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean delete(Word word) {
        String query = "DELETE FROM `vp_word` WHERE id = ?";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setInt(1, word.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /*
     * WORDSHOW DATABASE METHODS
     */

    public static boolean update(WordShow wordShow) {
        String query = "UPDATE `vp_word_show` SET showed_time = ?, show_counts = ? " +
                "WHERE word_id = ?";
        try {
            PreparedStatement statement = getConnection().prepareStatement(query);

            statement.setLong(1, wordShow.getShowedTime());
            statement.setInt(2, wordShow.getShowCount());
            statement.setInt(3, wordShow.getWordId());
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /*
     * LAST STATE DATABASE METHODS
     */

    public  static boolean save(long minutes) {
        String query = "UPDATE `vp_last_state` SET time = ?;";

        try {
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setLong(1, minutes);
            statement.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /*
     * SINGLE DATABASE METHODS
     */

    private static ResultSet query(String query) {
        ResultSet rs = null;
        try {
            Statement statement = getConnection().createStatement();
            rs = statement.executeQuery(query);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    /*
     * SELECT DATABASE METHODS
     */

    public static Settings getSettings() {
        ResultSet rs = query("SELECT * FROM `vp_settings`");
        return Factory.getAllSettings(rs);
    }

    public static ObservableList<Language> getLanguages() {
        ResultSet rs = query("SELECT * FROM `vp_language`");
        return Factory.getAllLanguages(rs);
    }

    public static Group getGroups(Settings settings) {
        ResultSet rs = query("SELECT g.id, g.root_group, gn.text FROM `vp_group` g\n" +
                "INNER JOIN `vp_group_name` gn ON g.id = gn.group_id\n" +
                "WHERE gn.lang_id = " + settings.get(settings.LEFT_LANGUAGE).getValue() + " " +
                "ORDER BY g.root_group");
        return Factory.getAllGroups(rs, settings);
    }

    public static ArrayList<Integer> getCurrentlySelectedGroups() {
        ResultSet rs = query("SELECT * FROM `vp_currently_selected_groups`");
        return Factory.getAllCurrentlySelectedGroups(rs);
    }
    
    public static ArrayList<Word> getWords(int groupId, Settings settings) {
        ResultSet rs = query("SELECT \n" +
                "    rw.left_word_id AS left_word_id,\n" +
                "    w2.text AS left_word_text,\n" +
                "    w2.picture AS left_word_picture,\n" +
                "    w2.lang_id AS left_word_lang_id,\n" +
                "    w2.comment AS left_word_comment,\n" +
                "    rw.right_word_id AS right_word_id,\n" +
                "    w.text AS right_word_text,\n" +
                "    w.picture AS right_word_picture,\n" +
                "    w.lang_id AS right_word_lang_id,\n" +
                "    w.comment AS right_word_comment\n" +
                "FROM `vp_word` w\n" +
                "INNER JOIN `vp_group_word` gw ON w.id = gw.word_id\n" +
                "INNER JOIN `vp_relation_word` rw ON rw.right_word_id = w.id\n" +
                "INNER JOIN `vp_word` w2 ON w2.id = rw.left_word_id\n" +
                "WHERE\n" +
                "    w.lang_id = " + settings.get(settings.RIGHT_LANGUAGE).getValue() + " " +
                "    AND \n" +
                "    gw.group_id = " + groupId + " " +
                "    AND\n" +
                "    w2.lang_id = " + settings.get(settings.LEFT_LANGUAGE).getValue() + " " +
                "ORDER BY rw.left_word_id");
        return Factory.getAllWords(rs);
    }

    public static ArrayList<Integer> getWordShowIntervals() {
        ResultSet rs = query("SELECT * FROM `vp_word_show_intervals`");
        return Factory.getAllWordShowIntervals(rs);
    }

    public static HashMap<Integer, WordShow> getWordsShow() {
        ResultSet rs = query("SELECT * FROM `vp_word_show`");
        return Factory.getAllWordsShow(rs);
    }

    public static long getSwitchedOffTime() {
        ResultSet rs = query("SELECT * FROM `vp_last_state`");
        return Factory.getAllSwitchedOffTime(rs);
    }

    /*
     * OBJECTS FACTORY FROM ResultSet
     */

    private static class Factory {

        private static Settings getAllSettings(ResultSet rs) {
            Settings settings = new Settings();
            try {
                while(rs.next()) {
                    Setting setting = new Setting(
                            rs.getInt("id"),
                            rs.getString("short_name"),
                            rs.getString("name"),
                            rs.getInt("value"),
                            rs.getInt("default_value")
                    );
                    settings.put(rs.getString("short_name"), setting);
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
            return settings;
        }

        private static ObservableList<Language> getAllLanguages(ResultSet rs) {
            ObservableList<Language> languageCollection = FXCollections.observableArrayList();
            try {
                while(rs.next()) {
                    Language language = new Language(
                            rs.getInt("id"),
                            rs.getString("short_name"),
                            rs.getString("name")
                    );
                    languageCollection.add(language);
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
            return languageCollection;
        }

        private static Group getAllGroups(ResultSet rs, Settings settings) {
            Group rootGroup = new Group(0, "root");
            try {
                while(rs.next()) {
                    Group subGroup = new Group(
                            rs.getInt("id"),
                            rs.getString("text")
                    );
                    subGroup.initWords();
                    subGroup.getWords().addAll(getWords(subGroup.getId(), settings));

                    recursiveMakeGroups(rootGroup, subGroup, rs.getInt("root_group"));
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
            return rootGroup;
        }

        private static void recursiveMakeGroups(Group currentGroup, Group group, int rootGroupId) {
            if(currentGroup.getId() == rootGroupId) {
                if(currentGroup.getSubGroups() == null) {
                    currentGroup.initSubGroups();
                }
                currentGroup.getSubGroups().add(group);
            } else if(currentGroup.getSubGroups() != null) {
                for(int i = 0; i < currentGroup.getSubGroups().size(); i++) {
                    recursiveMakeGroups(currentGroup.getSubGroups().get(i), group, rootGroupId);
                }
            }
        }

        private static ArrayList<Integer> getAllCurrentlySelectedGroups(ResultSet rs) {
            ArrayList<Integer> currentlySelectedGroups = new ArrayList<>();
            try {
                while(rs.next()) {
                    currentlySelectedGroups.add(rs.getInt("group_id"));
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
            return currentlySelectedGroups;
        }

        private static ArrayList<Word> getAllWords(ResultSet rs) {
            ArrayList<Word> words = new ArrayList<>();
            Word tempWord = new Word();
            tempWord.initTranslations();

            int i = 0;
            int oldWordId = 0;
            Word leftWord = null;
            try {
                while(rs.next()) {
                    Word rightWord = new Word(
                            rs.getInt("right_word_id"),
                            rs.getString("right_word_text"),
                            rs.getString("right_word_comment"),
                            rs.getBytes("right_word_picture"),
                            rs.getInt("right_word_lang_id")
                    );

                    if(oldWordId != rs.getInt("left_word_id")) {
                        oldWordId = rs.getInt("left_word_id");

                        leftWord = new Word(
                                rs.getInt("left_word_id"),
                                rs.getString("left_word_text"),
                                rs.getString("left_word_comment"),
                                rs.getBytes("left_word_picture"),
                                rs.getInt("left_word_lang_id")
                        );
                        leftWord.initTranslations();
                        words.add(leftWord);
                    }

                    tempWord.getTranslations().add(rightWord);

                    if(leftWord != null) {
                        leftWord.getTranslations().add(tempWord.getTranslations().get(i++));
                    }
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
            return words;
        }

        private static ArrayList<Integer> getAllWordShowIntervals(ResultSet rs) {
            ArrayList<Integer> intervals = new ArrayList<>();
            try {
                while(rs.next()) {
                    intervals.add(rs.getInt("show_count"), rs.getInt("interval"));
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
            return intervals;
        }

        private static HashMap<Integer, WordShow> getAllWordsShow(ResultSet rs) {
            HashMap<Integer, WordShow> wordShowCollection = new HashMap<>();
            try {
                while(rs.next()) {
                    WordShow wordShow = new WordShow(
                            rs.getInt("word_id"),
                            rs.getInt("showed_time"),
                            rs.getInt("show_counts")
                    );
                    wordShowCollection.put(wordShow.getWordId(), wordShow);
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
            return wordShowCollection;
        }

        private static long getAllSwitchedOffTime(ResultSet rs) {
            long switchedOffTime = 0;
            try {
                while(rs.next()) {
                    switchedOffTime = rs.getLong("time");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return switchedOffTime;
        }
    }
}
